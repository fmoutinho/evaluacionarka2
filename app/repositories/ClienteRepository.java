package repositories;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import models.Cliente;
import play.db.Database;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.sql.*;
import java.util.*;

import play.db.*;

import models.Cliente;

@Singleton
public class ClienteRepository {
    @Inject
    Database database;
    public List<Cliente> findAll() throws SQLException {
        PreparedStatement preparedStatement = database.getConnection().prepareStatement("select * from cliente");

        List<Cliente> clientes = new ArrayList<>();

        preparedStatement.execute();

        ResultSet rs = preparedStatement.getResultSet();

        while (rs.next()) {
            Cliente cliente = new Cliente();

            cliente.setIdCliente(rs.getInt("idCliente"));
            cliente.setNombre(rs.getString("nombre"));
            cliente.setApellido(rs.getString("apellido"));
            cliente.setDni(rs.getString("dni"));
            cliente.setTelefono(rs.getString("telefono"));
            cliente.setEmail(rs.getString("email"));
            clientes.add(cliente);
        }

        return clientes;
    }

    public Optional<Cliente> findById(int id) throws SQLException {
        PreparedStatement preparedStatement = database.getConnection().prepareStatement("select * from cliente where idCliente = ?");
        preparedStatement.setInt(1, id);
        preparedStatement.execute();
        ResultSet rs = preparedStatement.getResultSet();
        if (rs.next()) {
            Cliente cliente = new Cliente();
            cliente.setIdCliente(rs.getInt("idCliente"));
            cliente.setNombre(rs.getString("nombre"));
            cliente.setApellido(rs.getString("apellido"));
            cliente.setDni(rs.getString("dni"));
            cliente.setTelefono(rs.getString("telefono"));
            cliente.setEmail(rs.getString("email"));
            return Optional.of(cliente);
        }

        return Optional.empty();
    }
    public Optional<Cliente> findallParametrizada(String apellido) throws SQLException {
        PreparedStatement preparedStatement = database.getConnection().prepareStatement("select * from cliente where apellido=?");

        preparedStatement.setString(1, apellido);
        preparedStatement.execute();
        ResultSet rs = preparedStatement.getResultSet();
        if (rs.next()) {
            Cliente cliente = new Cliente();
            cliente.setIdCliente(rs.getInt("idCliente"));
            cliente.setNombre(rs.getString("nombre"));
            cliente.setApellido(rs.getString("apellido"));
            cliente.setDni(rs.getString("dni"));
            cliente.setTelefono(rs.getString("telefono"));
            cliente.setEmail(rs.getString("email"));
            return Optional.of(cliente);
        }

        return Optional.empty();
    }

    public void delete(int id) throws SQLException {
        PreparedStatement preparedStatement = database.getConnection().prepareStatement("delete from cliente where idCliente = ?");

        preparedStatement.setInt(1, id);

        preparedStatement.execute();
    }

    public void add(Cliente cliente) throws SQLException {
        // Book has no id because we are adding a new one to the database
        PreparedStatement preparedStatement = database.getConnection().prepareStatement("insert into cliente (idCliente, nombre, apellido,dni,telefono,email) values (null, ?, ?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, cliente.getNombre());
        preparedStatement.setString(2, cliente.getApellido());
        preparedStatement.setString(3, cliente.getDni());
        preparedStatement.setString(4, cliente.getTelefono());
        preparedStatement.setString(5, cliente.getEmail());
        preparedStatement.execute();
        // after inserting we want to get the generated id
        ResultSet rs = preparedStatement.getGeneratedKeys();
        if (rs.next()) {
            cliente.setIdCliente(rs.getInt(1));
        }
        preparedStatement.executeUpdate();
    }
    public void update(Cliente cliente) throws SQLException {
        PreparedStatement preparedStatement = database.getConnection().prepareStatement("update cliente set nombre = ?, dni = ? where idCliente = ?");
        preparedStatement.setString(1, cliente.getNombre());
        preparedStatement.setString(2, cliente.getDni());
        preparedStatement.setInt(3, cliente.getIdCliente());
        preparedStatement.executeUpdate();
    }

}
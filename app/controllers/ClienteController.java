package controllers;

import com.google.inject.Inject;
import play.db.Database;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import models.*;
import repositories.*;


public class ClienteController extends Controller {
    @Inject public ClienteRepository clienteRepository;
    @Inject Database database;

    public Result findAll() {
        return database.withConnection(conn -> {
            return ok(Json.toJson(clienteRepository.findAll())).as("application/json");
        });
    }

    public Result findById(int id) {
        return database.withConnection(conn -> {
            return ok(Json.toJson(clienteRepository.findById(id))).as("application/json");
        });
    }
    public Result findallParametrizada(String apellido) {
        return database.withConnection(conn -> {
            return ok(Json.toJson(clienteRepository.findallParametrizada(apellido))).as("application/json");
        });
    }
    public Result create() {
        return database.withConnection(conn -> {
            Cliente clienteRequest = Json.fromJson(request().body().asJson(), Cliente.class);

            clienteRepository.add(clienteRequest);

            return ok(Json.toJson(clienteRequest)).as("application/json");
        });
    }
    public Result delete(int id) {
        return database.withConnection(conn -> {
            clienteRepository.delete(id);
            return ok("{}").as("application/json");
        });
    }
    public Result update(int id) {
        return database.withConnection(conn -> {
            Cliente clienteRequest = Json.fromJson(request().body().asJson(), Cliente.class);
            clienteRequest.setIdCliente(id);
            clienteRepository.update(clienteRequest);
            return ok(Json.toJson(clienteRequest)).as("application/json");
        });
    }
}